# rdpwrap

## Enable multiple remote desktop connections via GPO

This can also be a Policy error. Maybe somehow you changed the Group Policy to allow multiple connections and get the RDP “The number of connections to this computer is limited” fixed.

Step 1. Click the Search button next to the start menu then enter “gpedit.msc”.

Edit Group Policy 

Step 2. Navigate here: Computer Configuration > Administrative Templates > Windows Components > Remote Desktop Services > Remote Desktop Session host > Connections.

Connection 

Step 3. On the right pane, find and double-click Limit Number of Connections.

Limit Number of Connections 

Step 4. You can either set it to Enabled and set the number to the limit you wish to have.

Unlimited Number 

Step 5. Again, on the right pane of Connections, find and double-click Restrict Remote Desktop Services users to a single Remote Desktop Services session.

Restrict Remote Desktop Services Users

Step 6. Set it to Disabled to turn off the user restrictions.

Disable Restrict Remote Desktop Services Users

Step 7. Reboot the computer to make the changes apply.